import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://media.discordapp.net/attachments/538268845379420166/934083446626725898/bicycle-1024x602.png'),
    NetworkImage('https://media.discordapp.net/attachments/538268845379420166/934083603971850240/sail-Boat.png'),
    NetworkImage('https://media.discordapp.net/attachments/538268845379420166/934083674545193041/unknown.png'),
    NetworkImage('https://media.discordapp.net/attachments/538268845379420166/934083740039262228/unknown.png'),
    NetworkImage('https://cdn.pixabay.com/photo/2012/04/18/23/03/railway-38185_960_720.png'),
    NetworkImage('https://media.discordapp.net/attachments/538268845379420166/934083873451683840/vector-running-human-icon-silhouette-with-shadow-isolated-white_1284-42615.png'),
    NetworkImage('https://media.discordapp.net/attachments/538268845379420166/934083946852003880/unknown.png'),
    NetworkImage('https://media.discordapp.net/attachments/538268845379420166/934084072152645632/unknown.png'),
    NetworkImage('https://media.discordapp.net/attachments/538268845379420166/934084126766661673/Health__2861_29.png'),
  ];

  // final icons = [
  //   Icons.directions_bike,
  //   Icons.directions_boat,
  //   Icons.directions_bus,
  //   Icons.directions_car,
  //   Icons.directions_railway,
  //   Icons.directions_run,
  //   Icons.directions_subway,
  //   Icons.directions_transit,
  //   Icons.directions_walk
  // ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('List View'),
          ),
          body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: images[index],
                    ),
                    title: Text(
                      '${titles[index]}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'There are many passengers in serveral vehicles',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    trailing: Icon(
                      Icons.notifications_none,
                      size: 25,
                    ),
                    onTap: () {
                      AlertDialog alert = AlertDialog(
                        title: Text(
                            'Welcome'), // To display the title it is optional
                        content: Text(
                            'This is a ${titles[index]}'), // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          ElevatedButton(
                            // FlatButton widget is used to make a text to work like a button

                            onPressed: () {
                              Navigator.of(context).pop();
                            }, // function used to perform after pressing the button
                            child: Text('CANCEL'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ACCEPT'),
                          ),
                        ],
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alert;
                        },
                      );
                    },
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          )),
    );
  }
}
